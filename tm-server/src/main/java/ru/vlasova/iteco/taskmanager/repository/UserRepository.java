package ru.vlasova.iteco.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.repository.IUserRepository;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class UserRepository implements IUserRepository {

    @NotNull
    private final Map<String, User> entities = new HashMap<>();

    @Override
    @Nullable
    public String checkUser(@NotNull final String login) {
        @Nullable final List<User> users = findAll();
        for (@Nullable User user : users) {
            if (login.equals(user.getLogin())) return user.getId();
        }
        return null;
    }

    @Override
    @Nullable
    public User findOne(@NotNull final String id) {
        return entities.get(id);
    }

    @Override
    @NotNull
    public User persist(@NotNull final User entity) throws DuplicateException {
        if (entities.containsValue(entity)) throw new DuplicateException("Such entity exists.");
        entities.put(entity.getId(), entity);
        return entity;
    }

    @Override
    public void merge(@NotNull final User entity) {
        entities.put(entity.getId(), entity);
    }

    @Override
    public void removeAll() {
        entities.clear();
    }

    @Override
    @NotNull
    public List<User> findAll() {
        return new ArrayList<>(entities.values());
    }

    @Override
    public void remove(@NotNull final String id) {
        entities.remove(id);
    }

    @Override
    public int count() {
        return entities.size();
    }
}