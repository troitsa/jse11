package ru.vlasova.iteco.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.entity.Session;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.error.AccessDeniedException;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;

import java.util.List;

public interface ISessionService extends IService<Session> {

    void remove(@Nullable String userId, @Nullable String id);

    @Nullable
    Session findOne(@Nullable String userId, @Nullable String id);

    @NotNull
    Session create(@Nullable final String login, @Nullable final String pass) throws DuplicateException;

    @NotNull
    Session create(@NotNull Session session, @Nullable final User user) throws DuplicateException;

    void validate(@Nullable final Session session) throws AccessDeniedException;

    @Nullable
    List<Session> findAll();

    @Nullable
    Session findOne(@Nullable final String id);

    @Nullable
    Session persist(@Nullable final Session obj) throws DuplicateException;

    void merge(@Nullable final Session obj);

    void remove(@Nullable final String id);

    void removeAll();

}
