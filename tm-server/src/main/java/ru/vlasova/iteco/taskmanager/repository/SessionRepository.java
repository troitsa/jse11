package ru.vlasova.iteco.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.repository.ISessionRepository;
import ru.vlasova.iteco.taskmanager.entity.Session;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;

import java.util.*;

public final class SessionRepository implements ISessionRepository {

    @NotNull
    private final Map<String, Session> entities = new HashMap<>();

    @Override
    public @Nullable Session findOne(@NotNull String userId, @NotNull String id) {
        for (@NotNull final Session session : entities.values()) {
            if (id.equals(session.getId()) && userId.equals(session.getUserId())) return session;
        }
        return null;
    }

    @Override
    public void remove(@NotNull String userId, @NotNull String id) {
        for (@NotNull final Session session : entities.values()) {
            if (id.equals(session.getId()) && userId.equals(session.getUserId())) entities.remove(id);
        }
    }

    @Override
    public boolean contains(@NotNull final Session session) {
        @Nullable final Session checkSession = entities.get(session.getId());
        if (checkSession == null) return false;
        return Objects.equals(checkSession.getUserId(), session.getUserId())
                && Objects.equals(checkSession.getSignature(), session.getSignature())
                && Objects.equals(checkSession.getCreateDate(), session.getCreateDate())
                && Objects.equals(checkSession.getRole(), session.getRole());
    }

    @Override
    @Nullable
    public Session findOne(@NotNull final String id) {
        return entities.get(id);
    }

    @Override
    @NotNull
    public Session persist(@NotNull final Session entity) throws DuplicateException {
        if (entities.containsValue(entity)) throw new DuplicateException("Such entity exists.");
        entities.put(entity.getId(), entity);
        return entity;
    }

    @Override
    public void merge(@NotNull final Session entity) {
        entities.put(entity.getId(), entity);
    }

    @Override
    public void removeAll() {
        entities.clear();
    }

    @Override
    @NotNull
    public List<Session> findAll() {
        return new ArrayList<>(entities.values());
    }

    @Override
    public void remove(@NotNull final String id) {
        entities.remove(id);
    }

    @Override
    public int count() {
        return entities.size();
    }
}
