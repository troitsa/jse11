package ru.vlasova.iteco.taskmanager.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.NoArgsConstructor;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.endpoint.IDomainEndpoint;
import ru.vlasova.iteco.taskmanager.api.service.IDomainService;
import ru.vlasova.iteco.taskmanager.api.service.IPropertyService;
import ru.vlasova.iteco.taskmanager.api.service.ISessionService;
import ru.vlasova.iteco.taskmanager.entity.Domain;
import ru.vlasova.iteco.taskmanager.entity.Session;

import javax.jws.WebService;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.nio.file.Paths;

@NoArgsConstructor
@WebService(endpointInterface = "ru.vlasova.iteco.taskmanager.api.endpoint.IDomainEndpoint")
public class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    @Nullable
    private IDomainService domainService;

    @Nullable
    protected IPropertyService propertyService;

    public DomainEndpoint(@NotNull final ISessionService sessionService,
                          @NotNull final IDomainService domainService,
                          @NotNull final IPropertyService propertyService) {
        super(sessionService);
        this.domainService = domainService;
        this.propertyService = propertyService;
    }

    @Override
    @Nullable
    public String getSaveDir() {
        return propertyService.getSaveDir();
    }

    @Override
    public void binarySave(@Nullable final Session session) throws Exception {
        validateSession(session);
        @NotNull final Domain domain = new Domain();
        domainService.save(domain);
        @NotNull final File file = new File(getSaveDir() + File.separator + "data.bin");
        file.getParentFile().mkdirs();
        @NotNull final FileOutputStream outputStream = new FileOutputStream(file);
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
    }

    @Override
    public void binaryLoad(@Nullable final Session session) throws Exception {
        validateSession(session);
        @NotNull final FileInputStream inputStream = new FileInputStream(getSaveDir() + File.separator + "data.bin");
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
        @Nullable final Domain domain = (Domain) objectInputStream.readObject();
        domainService.load(domain);
        objectInputStream.close();
    }

    @Override
    public void fasterJsonLoad(@Nullable final Session session) throws Exception {
        validateSession(session);
        @NotNull final File file = new File(getSaveDir() + File.separator + "faster.json");
        file.getParentFile().mkdirs();
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final Domain domain = mapper.readValue(file, Domain.class);
        domainService.load(domain);
    }

    @Override
    public void fasterJsonSave(@Nullable final Session session) throws Exception {
        validateSession(session);
        @NotNull final Domain domain = new Domain();
        domainService.save(domain);
        @NotNull final File file = new File(getSaveDir() + File.separator + "faster.json");
        file.getParentFile().mkdirs();
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        mapper.writerWithDefaultPrettyPrinter().writeValue(file, domain);
    }

    @Override
    public void jaxBJsonLoad(@Nullable final Session session) throws Exception {
        validateSession(session);
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        unmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(Paths.get(getSaveDir() + File.separator + "jaxb.json").toFile());
        domainService.load(domain);
    }

    @Override
    public void jaxBJsonSave(@Nullable final Session session) throws Exception {
        validateSession(session);
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final Domain domain = new Domain();
        domainService.save(domain);
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        @NotNull final File file = new File(getSaveDir() + File.separator + "jaxb.json");
        file.getParentFile().mkdirs();
        @NotNull final FileOutputStream outputStream = new FileOutputStream(file);
        marshaller.marshal(domain, file);
        outputStream.close();
    }

    @Override
    public void fasterXmlLoad(@Nullable final Session session) throws Exception {
        validateSession(session);
        @NotNull final File file = new File(getSaveDir() + File.separator + "faster.xml");
        file.getParentFile().mkdirs();
        @NotNull final ObjectMapper mapper = new XmlMapper();
        @NotNull final Domain domain = mapper.readValue(file, Domain.class);
        domainService.load(domain);
    }

    @Override
    public void fasterXmlSave(@Nullable final Session session) throws Exception {
        validateSession(session);
        @NotNull final Domain domain = new Domain();
        domainService.save(domain);
        @NotNull final File file = new File(getSaveDir() + File.separator + "faster.xml");
        file.getParentFile().mkdirs();
        @NotNull final ObjectMapper mapper = new XmlMapper();
        mapper.writerWithDefaultPrettyPrinter().writeValue(file, domain);
    }

    @Override
    public void jaxBXmlLoad(@Nullable final Session session) throws Exception {
        validateSession(session);
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(Paths.get(getSaveDir() + File.separator + "jaxb.xml").toFile());
        domainService.load(domain);
    }

    @Override
    public void jaxBXmlSave(@Nullable final Session session) throws Exception {
        validateSession(session);
        @NotNull final Domain domain = new Domain();
        domainService.save(domain);
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        @NotNull final File file = new File(getSaveDir() + File.separator + "jaxb.xml");
        file.getParentFile().mkdirs();
        @NotNull final FileOutputStream outputStream = new FileOutputStream(file);
        marshaller.marshal(domain, file);
        outputStream.close();
    }

}
