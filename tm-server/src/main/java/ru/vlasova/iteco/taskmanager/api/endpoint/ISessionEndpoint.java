package ru.vlasova.iteco.taskmanager.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.entity.Session;
import ru.vlasova.iteco.taskmanager.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface ISessionEndpoint {

    @WebMethod
    void removeSession(@Nullable final String userId, @Nullable final String id);

    @Nullable
    @WebMethod
    Session findOneSession(@Nullable final String userId, @Nullable final String id);

    @Nullable
    @WebMethod
    Session createSessionNewUser(@NotNull final String login, @NotNull final String password) throws Exception;

    @Nullable
    @WebMethod
    Session createSession(@NotNull Session session, @Nullable final User user) throws Exception;

}
