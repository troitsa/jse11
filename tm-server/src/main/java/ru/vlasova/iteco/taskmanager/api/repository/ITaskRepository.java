package ru.vlasova.iteco.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;

import java.util.List;

public interface ITaskRepository {

    @Nullable
    Task getTaskByIndex(@NotNull final String userId, final int index);

    void removeTasksByProjectId(@NotNull final String userId, @NotNull final String projectId);

    @Nullable
    List<Task> getTasksByProjectId(@NotNull final String userId, @NotNull final String projectId);

    @NotNull
    List<Task> search(@NotNull String userId, @NotNull String searchString);

    @NotNull
    List<Task> findAll(@NotNull String userId);

    @Nullable
    Task findOneByUserId(@NotNull String userId, @NotNull String id);

    void removeAll(@NotNull String userId);

    void remove(@NotNull String userId, @NotNull String id);

    @NotNull
    List<Task> findAll();

    @Nullable
    Task findOne(@NotNull String id);

    @Nullable
    Task persist(@NotNull Task obj) throws DuplicateException;

    void merge(@NotNull Task obj);

    void remove(@NotNull String id);

    void removeAll();

    int count();

}
