package ru.vlasova.iteco.taskmanager.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.endpoint.ISessionEndpoint;
import ru.vlasova.iteco.taskmanager.api.service.ISessionService;
import ru.vlasova.iteco.taskmanager.api.service.IUserService;
import ru.vlasova.iteco.taskmanager.entity.Session;
import ru.vlasova.iteco.taskmanager.entity.User;

import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "ru.vlasova.iteco.taskmanager.api.endpoint.ISessionEndpoint")
public final class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    @NotNull private ISessionService sessionService;

    @NotNull private IUserService userService;

    public SessionEndpoint(@NotNull ISessionService sessionService, @NotNull IUserService userService) {
        this.sessionService = sessionService;
        this.userService = userService;
    }

    @Override
    public void removeSession(@Nullable final String userId, @Nullable final String id) {
        sessionService.remove(userId, id);
    }

    @Override
    public @Nullable Session findOneSession(@Nullable final String userId, @Nullable final String id) {
        return sessionService.findOne(userId, id);
    }

    @Override
    public @Nullable Session createSessionNewUser(@NotNull final String login, @NotNull final String password) throws Exception {
        return sessionService.create(login, password);
    }

    @Override
    @Nullable
    public Session createSession(@NotNull Session session, @Nullable User user) throws Exception {
        return sessionService.create(session, user);
    }

}
