package ru.vlasova.iteco.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;

import java.util.List;

public interface IProjectRepository {

    @Nullable
    String getIdByIndex(@NotNull final String userId, final int projectIndex);

    @NotNull
    List<Project> search(@NotNull String userId, @NotNull String searchString);

    @NotNull
    List<Project> findAll(@NotNull String userId);

    void removeAll(@NotNull String userId);

    @Nullable
    Project findOneByUserId(@NotNull String userId, @NotNull String id);

    void remove(@NotNull String userId, @NotNull String id);

    @NotNull
    List<Project> findAll();

    @Nullable
    Project findOne(@NotNull String id);

    @Nullable
    Project persist(@NotNull Project obj) throws DuplicateException;

    void merge(@NotNull Project obj);

    void remove(@NotNull String id);

    void removeAll();

    int count();

}
