package ru.vlasova.iteco.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull
    String getServerHost();

    @NotNull
    Integer getServerPort();

    @NotNull
    String getSessionSalt();

    @NotNull
    Integer getSessionCycle();

    @NotNull
    Integer getSessionLifetime();

    @NotNull
    String getSaveDir();
}
