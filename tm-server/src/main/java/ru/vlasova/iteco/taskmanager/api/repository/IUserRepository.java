package ru.vlasova.iteco.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;

import java.util.List;

public interface IUserRepository {

    @Nullable
    String checkUser(@NotNull final String login);

    @NotNull
    List<User> findAll();

    @Nullable
    User findOne(@NotNull String id);

    @Nullable
    User persist(@NotNull User obj) throws DuplicateException;

    void merge(@NotNull User obj);

    void remove(@NotNull String id);

    void removeAll();

    int count();

}
