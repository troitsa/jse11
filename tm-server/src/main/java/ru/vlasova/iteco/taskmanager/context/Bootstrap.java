package ru.vlasova.iteco.taskmanager.context;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import ru.vlasova.iteco.taskmanager.api.endpoint.*;
import ru.vlasova.iteco.taskmanager.api.repository.IProjectRepository;
import ru.vlasova.iteco.taskmanager.api.repository.ISessionRepository;
import ru.vlasova.iteco.taskmanager.api.repository.ITaskRepository;
import ru.vlasova.iteco.taskmanager.api.repository.IUserRepository;
import ru.vlasova.iteco.taskmanager.api.service.*;
import ru.vlasova.iteco.taskmanager.endpoint.*;
import ru.vlasova.iteco.taskmanager.repository.ProjectRepository;
import ru.vlasova.iteco.taskmanager.repository.SessionRepository;
import ru.vlasova.iteco.taskmanager.repository.TaskRepository;
import ru.vlasova.iteco.taskmanager.repository.UserRepository;
import ru.vlasova.iteco.taskmanager.service.*;

import javax.xml.ws.Endpoint;
import java.io.IOException;

public class Bootstrap {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository, taskService);

    @Getter
    @NotNull
    private final PropertyService propertyService =  new PropertyService();

    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository, userService, propertyService);

    @Getter
    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(sessionService, projectService);

    @Getter
    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(sessionService, taskService);

    @Getter
    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(sessionService, userService);

    @Getter
    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(sessionService, userService);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(userService, taskService, projectService);

    @Getter
    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(sessionService, domainService, propertyService);

    public Bootstrap() throws IOException {
    }

    public void start() throws Exception {
        Endpoint.publish("http://localhost:8080/project?wsdl", projectEndpoint);
        Endpoint.publish("http://localhost:8080/task?wsdl", taskEndpoint);
        Endpoint.publish("http://localhost:8080/user?wsdl", userEndpoint);
        Endpoint.publish("http://localhost:8080/session?wsdl", sessionEndpoint);
        Endpoint.publish("http://localhost:8080/domain?wsdl", domainEndpoint);
    }

}
