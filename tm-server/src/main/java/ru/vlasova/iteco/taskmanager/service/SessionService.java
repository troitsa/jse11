package ru.vlasova.iteco.taskmanager.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.repository.ISessionRepository;
import ru.vlasova.iteco.taskmanager.api.service.IPropertyService;
import ru.vlasova.iteco.taskmanager.api.service.ISessionService;
import ru.vlasova.iteco.taskmanager.api.service.IUserService;
import ru.vlasova.iteco.taskmanager.entity.Session;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.error.AccessDeniedException;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;
import ru.vlasova.iteco.taskmanager.util.SignatureUtil;

import java.util.List;

@Setter
public final class SessionService implements ISessionService {

    @NotNull
    @Getter
    private ISessionRepository repository;

    @NotNull
    private IUserService userService;

    @NotNull
    @Getter
    private IPropertyService propertyService;

    public SessionService(@NotNull final ISessionRepository repository,
                          @NotNull final IUserService userService,
                          @NotNull final IPropertyService propertyService) {
        this.repository = repository;
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @Override
    public void remove(@Nullable String userId, @Nullable String id) {
        if (userId == null || id == null) return;
        repository.remove(userId, id);
    }

    @Override
    @Nullable
    public Session findOne(@Nullable String userId, @Nullable String id) {
        if (userId == null || id == null) return null;
        return repository.findOne(userId, id);
    }

    @Override
    @Nullable
    public Session create(@Nullable final String login, @Nullable final String pass) throws DuplicateException {
        if (login == null || login.isEmpty()) return null;
        if (pass == null || pass.isEmpty()) return null;
        @Nullable final User user = userService.doLogin(login, pass);
        if (user == null) return null;
        @NotNull final Session session = new Session();
        session.setRole(user.getRole());
        session.setUserId(user.getId());
        session.setSignature(SignatureUtil.sign(session, propertyService.getSessionSalt(), propertyService.getSessionCycle()));
        return repository.persist(session);
    }

    @Override
    @Nullable
    public Session create(@NotNull Session session, @Nullable final User user) throws DuplicateException {
        if (user == null) return null;
        session.setRole(user.getRole());
        session.setUserId(user.getId());
        session.setSignature(SignatureUtil.sign(session, propertyService.getSessionSalt(), propertyService.getSessionCycle()));
        return repository.persist(session);
    }

    @Override
    public void validate(@Nullable final Session session) throws AccessDeniedException {
        if (session == null) throw new AccessDeniedException("Access denied!");
        if (session.getUserId() == null || session.getSignature() == null ||
                session.getRole() == null) throw new AccessDeniedException("Access denied!");
        if (!repository.contains(session)) throw new AccessDeniedException("Access denied!");
        final long existTime = System.currentTimeMillis() - session.getCreateDate().getTime();
        if (existTime > propertyService.getSessionLifetime()) {
            repository.remove(session.getId());
            throw new AccessDeniedException("Access denied! Session has expired.");
        }
    }

    @Override
    public void merge(@Nullable final Session entity) {
        if (entity == null) return;
        getRepository().merge(entity);
    }

    @Override
    @Nullable
    public Session persist(@Nullable final Session entity) throws DuplicateException {
        if (entity == null) return null;
        getRepository().persist(entity);
        return entity;
    }

    @Override
    @Nullable
    public List<Session> findAll() {
        return getRepository().findAll();
    }

    @Override
    @Nullable
    public Session findOne(@Nullable final String id) {
        if (id == null) return null;
        return getRepository().findOne(id);
    }

    @Override
    public void remove(@Nullable final String id) {
        if (id == null || id.trim().length() == 0) return;
        getRepository().remove(id);
    }

    @Override
    public void removeAll() {
        getRepository().removeAll();
    }

}
