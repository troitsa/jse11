package ru.vlasova.iteco.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.entity.Session;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;

import java.util.List;

public interface ISessionRepository {

    void remove(@NotNull String userId, @NotNull String id);

    @Nullable
    Session findOne(@NotNull String userId, @NotNull String id);

    boolean contains(@NotNull final Session session);

    @NotNull
    List<Session> findAll();

    @Nullable
    Session findOne(@NotNull String id);

    @Nullable
    Session persist(@NotNull Session obj) throws DuplicateException;

    void merge(@NotNull Session obj);

    void remove(@NotNull String id);

    void removeAll();

    int count();

}
