package ru.vlasova.iteco.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.repository.ITaskRepository;
import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class TaskRepository implements ITaskRepository {

    @NotNull
    private final Map<String, Task> entities = new HashMap<>();

    @Override
    @Nullable
    public Task getTaskByIndex(@NotNull final String userId, final int index) {
        return findAll(userId).get(index);
    }

    @Override
    public void removeTasksByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @Nullable final List<Task> taskList = getTasksByProjectId(userId, projectId);
        for (@Nullable Task task : taskList) {
            remove(task.getId());
        }
    }

    @Override
    @NotNull
    public List<Task> getTasksByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final List<Task> taskList = new ArrayList<>();
        @Nullable final List<Task> tasks = findAll(userId);
        for (@NotNull Task task : tasks) {
            if (task.getProjectId().equals(projectId)) {
                taskList.add(task);
            }
        }
        return taskList;
    }

    @Override
    @NotNull
    public List<Task> search(@NotNull final String userId, @NotNull final String searchString) {
        @Nullable final List<Task> taskList = findAll(userId);
        @NotNull final List<Task> taskSearch = new ArrayList<>();
        for (@NotNull Task task : taskList) {
            if (task.getName().contains(searchString) || task.getDescription().contains(searchString)) {
                taskSearch.add(task);
            }
        }
        return taskSearch;
    }

    @NotNull
    public List<Task> findAll(@NotNull final String userId) {
        @Nullable final List<Task> entityList = new ArrayList<>();
        for (@NotNull Task entity : entities.values()) {
            if (entity.getUserId() != null && entity.getUserId().equals(userId)) {
                entityList.add(entity);
            }
        }
        return entityList;
    }

    @Nullable
    public Task findOneByUserId(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Task entity = entities.get(id);
        if (entity == null) return null;
        if (userId.equals(entity.getUserId())) return entity;
        return null;
    }

    public void remove(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Task entity = entities.get(id);
        if (userId.equals(entity != null ? entity.getUserId() : null)) {
            entities.remove(id);
        }
    }

    public void removeAll(@NotNull final String userId) {
        @Nullable final List<Task> entities = findAll(userId);
        for (@NotNull Task entity : entities) {
            remove(userId, entity.getId());
        }
    }

    @Override
    @Nullable
    public Task findOne(@NotNull final String id) {
        return entities.get(id);
    }

    @Override
    @NotNull
    public Task persist(@NotNull final Task entity) throws DuplicateException {
        if (entities.containsValue(entity)) throw new DuplicateException("Such entity exists.");
        entities.put(entity.getId(), entity);
        return entity;
    }

    @Override
    public void merge(@NotNull final Task entity) {
        entities.put(entity.getId(), entity);
    }

    @Override
    public void removeAll() {
        entities.clear();
    }

    @Override
    @NotNull
    public List<Task> findAll() {
        return new ArrayList<>(entities.values());
    }

    @Override
    public void remove(@NotNull final String id) {
        entities.remove(id);
    }

    @Override
    public int count() {
        return entities.size();
    }
}
