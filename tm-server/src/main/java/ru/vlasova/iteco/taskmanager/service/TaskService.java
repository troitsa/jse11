package ru.vlasova.iteco.taskmanager.service;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.repository.ITaskRepository;
import ru.vlasova.iteco.taskmanager.api.service.ITaskService;
import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;
import ru.vlasova.iteco.taskmanager.util.DateUtil;

import java.util.List;


public final class TaskService implements ITaskService {

    @NotNull
    @Getter
    private final ITaskRepository repository;

    public TaskService(@NotNull ITaskRepository repository) {
        this.repository = repository;
    }

    @Override
    @Nullable
    public Task insert(@Nullable final String userId, @Nullable final String name,
                       @Nullable final String description, @Nullable final String dateStart,
                       @Nullable final String dateFinish) {
        final boolean checkGeneral = isValid(name, description, dateStart, dateFinish);
        if (!checkGeneral || userId == null) return null;
        @NotNull final Task task = new Task(userId);
        task.setName(name);
        task.setDescription(description);
        task.setDateStart(DateUtil.parseDateFromString(dateStart));
        task.setDateFinish(DateUtil.parseDateFromString(dateFinish));
        return task;
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String taskId) {
        if (taskId == null || userId == null) return;
        repository.remove(taskId);
    }

    @Override
    public void remove(@Nullable final String userId, final int id) {
        if (userId == null || id < 0) return;
        @Nullable final Task task = getTaskByIndex(userId, id);
        if (task == null) return;
        remove(userId, task.getId());
    }

    @Override
    @Nullable
    public Task getTaskByIndex(@Nullable final String userId, final int index) {
        if (userId == null || index < 0) return null;
        return repository.getTaskByIndex(userId, index);
    }

    @Override
    public void removeTasksByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || projectId == null) return;
        @Nullable final List<Task> taskList = getTasksByProjectId(userId, projectId);
        if (taskList == null) return;
        for (@NotNull Task task : taskList) {
            repository.remove(task.getId());
        }
    }

    @Override
    @Nullable
    public List<Task> getTasksByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || projectId == null || projectId.trim().isEmpty()) return null;
        @Nullable final List<Task> taskList = repository.getTasksByProjectId(userId, projectId);
        return taskList;
    }

    @Override
    @Nullable
    public List<Task> search(@Nullable final String userId, @Nullable final String searchString) {
        if (userId == null || searchString == null || searchString.trim().isEmpty()) return null;
        @NotNull final List<Task> taskList = repository.search(userId, searchString);
        return taskList;
    }

    @Override
    @Nullable
    public List<Task> findAll(@Nullable final String userId) {
        if (userId == null) return null;
        return getRepository().findAll(userId);
    }

    @Override
    public void merge(@Nullable final Task entity) {
        if (entity == null) return;
        getRepository().merge(entity);
    }

    @Override
    @Nullable
    public Task persist(@Nullable final Task entity) throws DuplicateException {
        if (entity == null) return null;
        getRepository().persist(entity);
        return entity;
    }

    @Override
    @Nullable
    public List<Task> findAll() {
        return getRepository().findAll();
    }

    @Override
    @Nullable
    public Task findOne(@Nullable final String id) {
        if (id == null) return null;
        return getRepository().findOne(id);
    }

    @Override
    @Nullable
    public Task findOneByUserId(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        return getRepository().findOneByUserId(userId, id);
    }

    @Override
    public void remove(@Nullable final String id) {
        if (id == null || id.trim().length() == 0) return;
        getRepository().remove(id);
    }

    @Override
    public void removeAll() {
        getRepository().removeAll();
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null) return;
        getRepository().removeAll(userId);
    }

}
