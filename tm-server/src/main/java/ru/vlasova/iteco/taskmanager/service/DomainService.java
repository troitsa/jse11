package ru.vlasova.iteco.taskmanager.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.service.IDomainService;
import ru.vlasova.iteco.taskmanager.api.service.IProjectService;
import ru.vlasova.iteco.taskmanager.api.service.ITaskService;
import ru.vlasova.iteco.taskmanager.api.service.IUserService;
import ru.vlasova.iteco.taskmanager.entity.Domain;
import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;

import java.util.List;

@RequiredArgsConstructor
public class DomainService implements IDomainService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final ITaskService taskService;

    @NotNull
    private final IProjectService projectService;

    @Override
    public void save(@NotNull final Domain domain) {
        domain.setUsers(userService.findAll());
        domain.setProjects(projectService.findAll());
        domain.setTasks(taskService.findAll());
    }

    @Override
    public void load(@NotNull final Domain domain) throws DuplicateException {
        @Nullable final List<User> userList = domain.getUsers();
        @Nullable final List<Project> projectList = domain.getProjects();
        @Nullable final List<Task> taskList = domain.getTasks();
        userService.removeAll();

        if (projectList != null) {
            for (Project project : projectList) {
                projectService.persist(project);
            }
        }

        if (taskList != null) {
            for (Task task : taskList) {
                taskService.persist(task);
            }
        }

        if (userList != null) {
            for (User user : userList) {
                userService.persist(user);
            }
        }
    }
}
