package ru.vlasova.iteco.taskmanager.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.entity.Session;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.enumeration.Role;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IUserEndpoint {

    @Nullable
    @WebMethod
    User insertUser(@Nullable final String login, @Nullable final String password) throws Exception;

    @Nullable
    @WebMethod
    User doLogin(@Nullable final String login, @Nullable final String password);

    @Nullable
    @WebMethod
    String checkUser(@Nullable final Session session, @Nullable final String login) throws Exception;

    @WebMethod
    void editUser(@Nullable final Session session, @Nullable final User user, @Nullable final String login, @Nullable final String password) throws Exception;

    @Nullable
    @WebMethod
    List<User> findAllUsers(@Nullable final Session session) throws Exception;

    @Nullable
    @WebMethod
    User findUserBySession(@Nullable final Session session, @Nullable String id) throws Exception;

    @Nullable
    @WebMethod
    User findUser(@Nullable final String id);

    @Nullable
    @WebMethod
    User persistUser(@Nullable User user) throws Exception;

    @WebMethod
    void mergeUser(@Nullable final Session session, @Nullable User user) throws Exception;

    @WebMethod
    void removeUser(@Nullable final Session session, @Nullable String id) throws Exception;

    @WebMethod
    void removeAllUsers(@Nullable final Session session) throws Exception;

    @WebMethod
    Boolean checkRole(@Nullable final String userId, @NotNull final List<Role> roles);

}
