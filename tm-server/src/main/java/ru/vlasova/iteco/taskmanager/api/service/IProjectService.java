package ru.vlasova.iteco.taskmanager.api.service;

import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;

import java.util.List;

public interface IProjectService extends IService<Project> {

    @Nullable
    Project insert(@Nullable final String userId, @Nullable final String name,
                   @Nullable final String description, @Nullable final String dateStart,
                   @Nullable final String dateFinish);

    @Nullable
    Project getProjectByIndex(@Nullable final String userId, int index);

    void remove(@Nullable final String userId, @Nullable final String id);

    void remove(@Nullable final String userId, int index);

    @Nullable
    List<Task> getTasksByProjectIndex(@Nullable final String userId, int projectIndex);

    @Nullable
    List<Project> search(@Nullable final String userId, @Nullable final String searchString);

    @Nullable
    List<Project> findAll();

    @Nullable
    List<Project> findAll(@Nullable final String userId);

    @Nullable
    Project findOne(@Nullable final String id);

    @Nullable
    Project findOneByUserId(@Nullable final String userId, @Nullable final String id);

    @Nullable
    Project persist(@Nullable final Project obj) throws DuplicateException;

    void merge(@Nullable final Project obj);

    void remove(@Nullable final String id);

    void removeAll();

    void removeAll(@Nullable final String userId);

}
