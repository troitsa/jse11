package ru.vlasova.iteco.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.repository.IProjectRepository;
import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class ProjectRepository implements IProjectRepository {

    @NotNull
    private final Map<String, Project> entities = new HashMap<>();

    @Override
    @Nullable
    public String getIdByIndex(@NotNull final String userId, int projectIndex) {
        @Nullable final List<Project> projectList = findAll(userId);
        @Nullable final Project project = projectList.get(projectIndex);
        if (project == null) return null;
        @NotNull final String projectId = project.getId();
        return projectId;
    }

    @Override
    @NotNull
    public List<Project> search(@NotNull final String userId, @NotNull final String searchString) {
        @Nullable final List<Project> projectList = findAll(userId);
        @NotNull final List<Project> projectSearch = new ArrayList<>();
        for (@NotNull Project project : projectList) {
            if (project.getName().contains(searchString) || project.getDescription().contains(searchString)) {
                projectSearch.add(project);
            }
        }
        return projectSearch;
    }

    @NotNull
    public List<Project> findAll(@NotNull final String userId) {
        @Nullable final List<Project> entityList = new ArrayList<>();
        for (@NotNull Project entity : entities.values()) {
            if (entity.getUserId() != null && entity.getUserId().equals(userId)) {
                entityList.add(entity);
            }
        }
        return entityList;
    }

    @Nullable
    public Project findOneByUserId(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Project entity = entities.get(id);
        if (entity == null) return null;
        if (userId.equals(entity.getUserId())) return entity;
        return null;
    }

    public void remove(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Project entity = entities.get(id);
        if (userId.equals(entity != null ? entity.getUserId() : null)) {
            entities.remove(id);
        }
    }

    public void removeAll(@NotNull final String userId) {
        @Nullable final List<Project> entities = findAll(userId);
        for (@NotNull Project entity : entities) {
            remove(userId, entity.getId());
        }
    }

    @Override
    @Nullable
    public Project findOne(@NotNull final String id) {
        return entities.get(id);
    }

    @Override
    @NotNull
    public Project persist(@NotNull final Project entity) throws DuplicateException {
        if (entities.containsValue(entity)) throw new DuplicateException("Such entity exists.");
        entities.put(entity.getId(), entity);
        return entity;
    }

    @Override
    public void merge(@NotNull final Project entity) {
        entities.put(entity.getId(), entity);
    }

    @Override
    public void removeAll() {
        entities.clear();
    }

    @Override
    @NotNull
    public List<Project> findAll() {
        return new ArrayList<>(entities.values());
    }

    @Override
    public void remove(@NotNull final String id) {
        entities.remove(id);
    }

    @Override
    public int count() {
        return entities.size();
    }

}
