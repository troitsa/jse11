package ru.vlasova.iteco.taskmanager.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.endpoint.*;
import ru.vlasova.iteco.taskmanager.api.service.ServiceLocator;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

import java.lang.Exception;
import java.util.List;

public abstract class AbstractCommand {

    protected ServiceLocator serviceLocator;

    protected Session session;

    protected String userId;

    public void setServiceLocator(final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract boolean secure();

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getDescription();

    public abstract void execute() throws Exception;

    public List<Role> getRole() {
        return null;
    }

    public void printProjectList(@Nullable final String userId) throws Exception {
        @NotNull final IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        @NotNull final TerminalService terminalService = serviceLocator.getTerminalService();
        @Nullable final List<Project> projectList = projectEndpoint.findAllProjectsByUserId(session, userId);
        if (projectList == null || projectList.size() == 0) {
            terminalService.print("There are no projects.");
            return;
        }
        int i = 1;
        for (@NotNull Project project : projectList) {
            terminalService.print(i++ + ": " + project.getName());
        }
    }

    public void printTaskList(@Nullable final List<Task> taskList) {
        int i = 1;
        if (taskList == null) return;
        for (@Nullable Task task : taskList) {
            serviceLocator.getTerminalService().print(i++ + ": " + task.getName());
        }
    }

    public void validSession(){
        session =  serviceLocator.getStateService().getSession();
        if(session == null) {
            serviceLocator.getTerminalService().print("Access denied");
            return;
        }
        userId = session.getUserId();
    }

}