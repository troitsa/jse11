package ru.vlasova.iteco.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.vlasova.iteco.taskmanager.api.endpoint.Session;
import ru.vlasova.iteco.taskmanager.api.endpoint.User;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

public final class UserLogInCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    @NotNull
    public String getName() {
        return "user_login";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "User authorization";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final IUserEndpoint userEndpoint = serviceLocator.getUserEndpoint();
        @NotNull final TerminalService terminalService = serviceLocator.getTerminalService();
        terminalService.print("Login: ");
        @Nullable final String login = terminalService.readString();
        terminalService.print("Password: ");
        @Nullable final String password = terminalService.readString();
        @Nullable final Session session = serviceLocator.getSessionEndpoint().createSessionNewUser(login, password);
        if (session == null) return;
        @Nullable final User user = userEndpoint.findUser(session.getUserId());
        if(user != null) {
            serviceLocator.getStateService().setSession(session);
            terminalService.print("Log in success");
        }
        else {
            terminalService.print("Login or password invalid");
        }
    }

}