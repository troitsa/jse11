package ru.vlasova.iteco.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.endpoint.ITaskEndpoint;
import ru.vlasova.iteco.taskmanager.api.endpoint.Role;
import ru.vlasova.iteco.taskmanager.api.endpoint.Task;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class TaskSearchCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public List<Role> getRole() {
        List<Role> roles = new ArrayList<>();
        roles.add(Role.USER);
        return roles;
    }

    @Override
    public @NotNull String getName() {
        return "task_search";
    }

    @Override
    public @NotNull String getDescription() {
        return "Search task by name or description";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        @NotNull final TerminalService terminalService = serviceLocator.getTerminalService();
        validSession();
        terminalService.print("Search task: ");
        @Nullable final String search = terminalService.readString();
        @Nullable final List<Task> taskList = taskEndpoint.searchTask(session, userId, search);
        if(taskList == null || taskList.isEmpty()) {
            terminalService.print("No matches");
            return;
        }
        Stream<Task> stream = taskList.stream();
        stream.forEach(e -> terminalService.print(e.getName()));
    }

}
