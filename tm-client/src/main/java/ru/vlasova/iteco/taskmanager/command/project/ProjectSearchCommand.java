package ru.vlasova.iteco.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.endpoint.IProjectEndpoint;
import ru.vlasova.iteco.taskmanager.api.endpoint.Project;
import ru.vlasova.iteco.taskmanager.api.endpoint.Role;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class ProjectSearchCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public List<Role> getRole() {
        List<Role> roles = new ArrayList<>();
        roles.add(Role.USER);
        return roles;
    }

    @Override
    public @NotNull String getName() {
        return "project_search";
    }

    @Override
    public @NotNull String getDescription() {
        return "Search project by name or description";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        @NotNull final TerminalService terminalService = serviceLocator.getTerminalService();
        validSession();
        terminalService.print("Search projects: ");
        @Nullable final String search = terminalService.readString();
        @Nullable final List<Project> projectList = projectEndpoint.searchProject(session, userId, search);
        if(projectList == null || projectList.isEmpty()) {
            terminalService.print("No matches");
            return;
        }
        Stream<Project> stream = projectList.stream();
        stream.forEach(e -> terminalService.print(e.getName()));
    }

}
