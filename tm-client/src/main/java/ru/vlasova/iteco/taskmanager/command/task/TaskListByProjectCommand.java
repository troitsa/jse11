package ru.vlasova.iteco.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.endpoint.IProjectEndpoint;
import ru.vlasova.iteco.taskmanager.api.endpoint.Role;
import ru.vlasova.iteco.taskmanager.api.endpoint.Task;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

import java.util.ArrayList;
import java.util.List;

public final class TaskListByProjectCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public List<Role> getRole() {
        List<Role> roles = new ArrayList<>();
        roles.add(Role.USER);
        return roles;
    }

    @Override
    @NotNull
    public String getName() {
        return "task_list_by_project";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show tasks by selected project";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        @NotNull final TerminalService terminalService = serviceLocator.getTerminalService();
        validSession();
        printProjectList(userId);
        terminalService.print("Please, choose the project and type the number");
        @Nullable final int index = Integer.parseInt(terminalService.readString())-1;
        @Nullable final List<Task> taskList = projectEndpoint.getTasksByProjectIndex(session, userId, index);
        if(taskList == null || taskList.size()==0) {
            terminalService.print("There are no tasks.");
        }
        else {
            printTaskList(taskList);
        }
    }

}