package ru.vlasova.iteco.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.endpoint.ITaskEndpoint;
import ru.vlasova.iteco.taskmanager.api.endpoint.Role;
import ru.vlasova.iteco.taskmanager.api.endpoint.Task;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

import java.util.ArrayList;
import java.util.List;


public final class TaskCreateCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public List<Role> getRole() {
        List<Role> roles = new ArrayList<>();
        roles.add(Role.USER);
        return roles;
    }

    @Override
    @NotNull
    public String getName() {
        return "task_create";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Create new task";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        @NotNull final TerminalService terminalService = serviceLocator.getTerminalService();
        validSession();
        terminalService.print("Creating task. Set name: ");
        @Nullable final String name = terminalService.readString();
        terminalService.print("Input description: ");
        @Nullable final String description = terminalService.readString();
        terminalService.print("Set start date: ");
        @Nullable final String dateStart = terminalService.readString();
        terminalService.print("Set end date: ");
        @Nullable final String dateFinish = terminalService.readString();
        @Nullable final Task task = taskEndpoint.insertTask(session, userId, name, description, dateStart, dateFinish);
        if(task == null) return;
        taskEndpoint.persistTask(session, task);
        terminalService.print("Task created.");
    }

}