package ru.vlasova.iteco.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import ru.vlasova.iteco.taskmanager.api.endpoint.Role;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

import java.util.ArrayList;
import java.util.List;

public final class UserLogOutCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public List<Role> getRole() {
        List<Role> roles = new ArrayList<>();
        roles.add(Role.USER);
        roles.add(Role.ADMIN);
        return roles;
    }

    @Override
    @NotNull
    public String getName() {
        return "user_logout";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Logout user";
    }

    @Override
    public void execute() {
        @NotNull final TerminalService terminalService = serviceLocator.getTerminalService();
        validSession();
        serviceLocator.getSessionEndpoint().removeSession(userId, session.getId());
        serviceLocator.getStateService().setSession(null);
        terminalService.print("Log out success");
    }

}