
package ru.vlasova.iteco.taskmanager.api.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.vlasova.iteco.taskmanager.api.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Exception_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "Exception");
    private final static QName _BinaryLoad_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "binaryLoad");
    private final static QName _BinaryLoadResponse_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "binaryLoadResponse");
    private final static QName _BinarySave_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "binarySave");
    private final static QName _BinarySaveResponse_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "binarySaveResponse");
    private final static QName _FasterJsonLoad_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "fasterJsonLoad");
    private final static QName _FasterJsonLoadResponse_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "fasterJsonLoadResponse");
    private final static QName _FasterJsonSave_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "fasterJsonSave");
    private final static QName _FasterJsonSaveResponse_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "fasterJsonSaveResponse");
    private final static QName _FasterXmlLoad_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "fasterXmlLoad");
    private final static QName _FasterXmlLoadResponse_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "fasterXmlLoadResponse");
    private final static QName _FasterXmlSave_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "fasterXmlSave");
    private final static QName _FasterXmlSaveResponse_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "fasterXmlSaveResponse");
    private final static QName _GetSaveDir_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "getSaveDir");
    private final static QName _GetSaveDirResponse_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "getSaveDirResponse");
    private final static QName _JaxBJsonLoad_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "jaxBJsonLoad");
    private final static QName _JaxBJsonLoadResponse_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "jaxBJsonLoadResponse");
    private final static QName _JaxBJsonSave_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "jaxBJsonSave");
    private final static QName _JaxBJsonSaveResponse_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "jaxBJsonSaveResponse");
    private final static QName _JaxBXmlLoad_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "jaxBXmlLoad");
    private final static QName _JaxBXmlLoadResponse_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "jaxBXmlLoadResponse");
    private final static QName _JaxBXmlSave_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "jaxBXmlSave");
    private final static QName _JaxBXmlSaveResponse_QNAME = new QName("http://endpoint.api.taskmanager.iteco.vlasova.ru/", "jaxBXmlSaveResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.vlasova.iteco.taskmanager.api.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link BinaryLoad }
     * 
     */
    public BinaryLoad createBinaryLoad() {
        return new BinaryLoad();
    }

    /**
     * Create an instance of {@link BinaryLoadResponse }
     * 
     */
    public BinaryLoadResponse createBinaryLoadResponse() {
        return new BinaryLoadResponse();
    }

    /**
     * Create an instance of {@link BinarySave }
     * 
     */
    public BinarySave createBinarySave() {
        return new BinarySave();
    }

    /**
     * Create an instance of {@link BinarySaveResponse }
     * 
     */
    public BinarySaveResponse createBinarySaveResponse() {
        return new BinarySaveResponse();
    }

    /**
     * Create an instance of {@link FasterJsonLoad }
     * 
     */
    public FasterJsonLoad createFasterJsonLoad() {
        return new FasterJsonLoad();
    }

    /**
     * Create an instance of {@link FasterJsonLoadResponse }
     * 
     */
    public FasterJsonLoadResponse createFasterJsonLoadResponse() {
        return new FasterJsonLoadResponse();
    }

    /**
     * Create an instance of {@link FasterJsonSave }
     * 
     */
    public FasterJsonSave createFasterJsonSave() {
        return new FasterJsonSave();
    }

    /**
     * Create an instance of {@link FasterJsonSaveResponse }
     * 
     */
    public FasterJsonSaveResponse createFasterJsonSaveResponse() {
        return new FasterJsonSaveResponse();
    }

    /**
     * Create an instance of {@link FasterXmlLoad }
     * 
     */
    public FasterXmlLoad createFasterXmlLoad() {
        return new FasterXmlLoad();
    }

    /**
     * Create an instance of {@link FasterXmlLoadResponse }
     * 
     */
    public FasterXmlLoadResponse createFasterXmlLoadResponse() {
        return new FasterXmlLoadResponse();
    }

    /**
     * Create an instance of {@link FasterXmlSave }
     * 
     */
    public FasterXmlSave createFasterXmlSave() {
        return new FasterXmlSave();
    }

    /**
     * Create an instance of {@link FasterXmlSaveResponse }
     * 
     */
    public FasterXmlSaveResponse createFasterXmlSaveResponse() {
        return new FasterXmlSaveResponse();
    }

    /**
     * Create an instance of {@link GetSaveDir }
     * 
     */
    public GetSaveDir createGetSaveDir() {
        return new GetSaveDir();
    }

    /**
     * Create an instance of {@link GetSaveDirResponse }
     * 
     */
    public GetSaveDirResponse createGetSaveDirResponse() {
        return new GetSaveDirResponse();
    }

    /**
     * Create an instance of {@link JaxBJsonLoad }
     * 
     */
    public JaxBJsonLoad createJaxBJsonLoad() {
        return new JaxBJsonLoad();
    }

    /**
     * Create an instance of {@link JaxBJsonLoadResponse }
     * 
     */
    public JaxBJsonLoadResponse createJaxBJsonLoadResponse() {
        return new JaxBJsonLoadResponse();
    }

    /**
     * Create an instance of {@link JaxBJsonSave }
     * 
     */
    public JaxBJsonSave createJaxBJsonSave() {
        return new JaxBJsonSave();
    }

    /**
     * Create an instance of {@link JaxBJsonSaveResponse }
     * 
     */
    public JaxBJsonSaveResponse createJaxBJsonSaveResponse() {
        return new JaxBJsonSaveResponse();
    }

    /**
     * Create an instance of {@link JaxBXmlLoad }
     * 
     */
    public JaxBXmlLoad createJaxBXmlLoad() {
        return new JaxBXmlLoad();
    }

    /**
     * Create an instance of {@link JaxBXmlLoadResponse }
     * 
     */
    public JaxBXmlLoadResponse createJaxBXmlLoadResponse() {
        return new JaxBXmlLoadResponse();
    }

    /**
     * Create an instance of {@link JaxBXmlSave }
     * 
     */
    public JaxBXmlSave createJaxBXmlSave() {
        return new JaxBXmlSave();
    }

    /**
     * Create an instance of {@link JaxBXmlSaveResponse }
     * 
     */
    public JaxBXmlSaveResponse createJaxBXmlSaveResponse() {
        return new JaxBXmlSaveResponse();
    }

    /**
     * Create an instance of {@link Session }
     * 
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BinaryLoad }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "binaryLoad")
    public JAXBElement<BinaryLoad> createBinaryLoad(BinaryLoad value) {
        return new JAXBElement<BinaryLoad>(_BinaryLoad_QNAME, BinaryLoad.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BinaryLoadResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "binaryLoadResponse")
    public JAXBElement<BinaryLoadResponse> createBinaryLoadResponse(BinaryLoadResponse value) {
        return new JAXBElement<BinaryLoadResponse>(_BinaryLoadResponse_QNAME, BinaryLoadResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BinarySave }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "binarySave")
    public JAXBElement<BinarySave> createBinarySave(BinarySave value) {
        return new JAXBElement<BinarySave>(_BinarySave_QNAME, BinarySave.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BinarySaveResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "binarySaveResponse")
    public JAXBElement<BinarySaveResponse> createBinarySaveResponse(BinarySaveResponse value) {
        return new JAXBElement<BinarySaveResponse>(_BinarySaveResponse_QNAME, BinarySaveResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FasterJsonLoad }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "fasterJsonLoad")
    public JAXBElement<FasterJsonLoad> createFasterJsonLoad(FasterJsonLoad value) {
        return new JAXBElement<FasterJsonLoad>(_FasterJsonLoad_QNAME, FasterJsonLoad.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FasterJsonLoadResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "fasterJsonLoadResponse")
    public JAXBElement<FasterJsonLoadResponse> createFasterJsonLoadResponse(FasterJsonLoadResponse value) {
        return new JAXBElement<FasterJsonLoadResponse>(_FasterJsonLoadResponse_QNAME, FasterJsonLoadResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FasterJsonSave }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "fasterJsonSave")
    public JAXBElement<FasterJsonSave> createFasterJsonSave(FasterJsonSave value) {
        return new JAXBElement<FasterJsonSave>(_FasterJsonSave_QNAME, FasterJsonSave.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FasterJsonSaveResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "fasterJsonSaveResponse")
    public JAXBElement<FasterJsonSaveResponse> createFasterJsonSaveResponse(FasterJsonSaveResponse value) {
        return new JAXBElement<FasterJsonSaveResponse>(_FasterJsonSaveResponse_QNAME, FasterJsonSaveResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FasterXmlLoad }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "fasterXmlLoad")
    public JAXBElement<FasterXmlLoad> createFasterXmlLoad(FasterXmlLoad value) {
        return new JAXBElement<FasterXmlLoad>(_FasterXmlLoad_QNAME, FasterXmlLoad.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FasterXmlLoadResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "fasterXmlLoadResponse")
    public JAXBElement<FasterXmlLoadResponse> createFasterXmlLoadResponse(FasterXmlLoadResponse value) {
        return new JAXBElement<FasterXmlLoadResponse>(_FasterXmlLoadResponse_QNAME, FasterXmlLoadResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FasterXmlSave }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "fasterXmlSave")
    public JAXBElement<FasterXmlSave> createFasterXmlSave(FasterXmlSave value) {
        return new JAXBElement<FasterXmlSave>(_FasterXmlSave_QNAME, FasterXmlSave.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FasterXmlSaveResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "fasterXmlSaveResponse")
    public JAXBElement<FasterXmlSaveResponse> createFasterXmlSaveResponse(FasterXmlSaveResponse value) {
        return new JAXBElement<FasterXmlSaveResponse>(_FasterXmlSaveResponse_QNAME, FasterXmlSaveResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSaveDir }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "getSaveDir")
    public JAXBElement<GetSaveDir> createGetSaveDir(GetSaveDir value) {
        return new JAXBElement<GetSaveDir>(_GetSaveDir_QNAME, GetSaveDir.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSaveDirResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "getSaveDirResponse")
    public JAXBElement<GetSaveDirResponse> createGetSaveDirResponse(GetSaveDirResponse value) {
        return new JAXBElement<GetSaveDirResponse>(_GetSaveDirResponse_QNAME, GetSaveDirResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link JaxBJsonLoad }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "jaxBJsonLoad")
    public JAXBElement<JaxBJsonLoad> createJaxBJsonLoad(JaxBJsonLoad value) {
        return new JAXBElement<JaxBJsonLoad>(_JaxBJsonLoad_QNAME, JaxBJsonLoad.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link JaxBJsonLoadResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "jaxBJsonLoadResponse")
    public JAXBElement<JaxBJsonLoadResponse> createJaxBJsonLoadResponse(JaxBJsonLoadResponse value) {
        return new JAXBElement<JaxBJsonLoadResponse>(_JaxBJsonLoadResponse_QNAME, JaxBJsonLoadResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link JaxBJsonSave }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "jaxBJsonSave")
    public JAXBElement<JaxBJsonSave> createJaxBJsonSave(JaxBJsonSave value) {
        return new JAXBElement<JaxBJsonSave>(_JaxBJsonSave_QNAME, JaxBJsonSave.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link JaxBJsonSaveResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "jaxBJsonSaveResponse")
    public JAXBElement<JaxBJsonSaveResponse> createJaxBJsonSaveResponse(JaxBJsonSaveResponse value) {
        return new JAXBElement<JaxBJsonSaveResponse>(_JaxBJsonSaveResponse_QNAME, JaxBJsonSaveResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link JaxBXmlLoad }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "jaxBXmlLoad")
    public JAXBElement<JaxBXmlLoad> createJaxBXmlLoad(JaxBXmlLoad value) {
        return new JAXBElement<JaxBXmlLoad>(_JaxBXmlLoad_QNAME, JaxBXmlLoad.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link JaxBXmlLoadResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "jaxBXmlLoadResponse")
    public JAXBElement<JaxBXmlLoadResponse> createJaxBXmlLoadResponse(JaxBXmlLoadResponse value) {
        return new JAXBElement<JaxBXmlLoadResponse>(_JaxBXmlLoadResponse_QNAME, JaxBXmlLoadResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link JaxBXmlSave }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "jaxBXmlSave")
    public JAXBElement<JaxBXmlSave> createJaxBXmlSave(JaxBXmlSave value) {
        return new JAXBElement<JaxBXmlSave>(_JaxBXmlSave_QNAME, JaxBXmlSave.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link JaxBXmlSaveResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "jaxBXmlSaveResponse")
    public JAXBElement<JaxBXmlSaveResponse> createJaxBXmlSaveResponse(JaxBXmlSaveResponse value) {
        return new JAXBElement<JaxBXmlSaveResponse>(_JaxBXmlSaveResponse_QNAME, JaxBXmlSaveResponse.class, null, value);
    }

}
