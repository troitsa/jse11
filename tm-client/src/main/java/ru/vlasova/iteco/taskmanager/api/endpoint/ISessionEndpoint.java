package ru.vlasova.iteco.taskmanager.api.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2020-05-16T21:25:15.167+03:00
 * Generated source version: 3.2.7
 *
 */
@WebService(targetNamespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", name = "ISessionEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface ISessionEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.api.taskmanager.iteco.vlasova.ru/ISessionEndpoint/createSessionNewUserRequest", output = "http://endpoint.api.taskmanager.iteco.vlasova.ru/ISessionEndpoint/createSessionNewUserResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.taskmanager.iteco.vlasova.ru/ISessionEndpoint/createSessionNewUser/Fault/Exception")})
    @RequestWrapper(localName = "createSessionNewUser", targetNamespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", className = "ru.vlasova.iteco.taskmanager.api.endpoint.CreateSessionNewUser")
    @ResponseWrapper(localName = "createSessionNewUserResponse", targetNamespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", className = "ru.vlasova.iteco.taskmanager.api.endpoint.CreateSessionNewUserResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.vlasova.iteco.taskmanager.api.endpoint.Session createSessionNewUser(
        @WebParam(name = "arg0", targetNamespace = "")
        java.lang.String arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        java.lang.String arg1
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.taskmanager.iteco.vlasova.ru/ISessionEndpoint/createSessionRequest", output = "http://endpoint.api.taskmanager.iteco.vlasova.ru/ISessionEndpoint/createSessionResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.taskmanager.iteco.vlasova.ru/ISessionEndpoint/createSession/Fault/Exception")})
    @RequestWrapper(localName = "createSession", targetNamespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", className = "ru.vlasova.iteco.taskmanager.api.endpoint.CreateSession")
    @ResponseWrapper(localName = "createSessionResponse", targetNamespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", className = "ru.vlasova.iteco.taskmanager.api.endpoint.CreateSessionResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.vlasova.iteco.taskmanager.api.endpoint.Session createSession(
        @WebParam(name = "arg0", targetNamespace = "")
        ru.vlasova.iteco.taskmanager.api.endpoint.Session arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        ru.vlasova.iteco.taskmanager.api.endpoint.User arg1
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.taskmanager.iteco.vlasova.ru/ISessionEndpoint/removeSessionRequest", output = "http://endpoint.api.taskmanager.iteco.vlasova.ru/ISessionEndpoint/removeSessionResponse")
    @RequestWrapper(localName = "removeSession", targetNamespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", className = "ru.vlasova.iteco.taskmanager.api.endpoint.RemoveSession")
    @ResponseWrapper(localName = "removeSessionResponse", targetNamespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", className = "ru.vlasova.iteco.taskmanager.api.endpoint.RemoveSessionResponse")
    public void removeSession(
        @WebParam(name = "arg0", targetNamespace = "")
        java.lang.String arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        java.lang.String arg1
    );

    @WebMethod
    @Action(input = "http://endpoint.api.taskmanager.iteco.vlasova.ru/ISessionEndpoint/findOneSessionRequest", output = "http://endpoint.api.taskmanager.iteco.vlasova.ru/ISessionEndpoint/findOneSessionResponse")
    @RequestWrapper(localName = "findOneSession", targetNamespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", className = "ru.vlasova.iteco.taskmanager.api.endpoint.FindOneSession")
    @ResponseWrapper(localName = "findOneSessionResponse", targetNamespace = "http://endpoint.api.taskmanager.iteco.vlasova.ru/", className = "ru.vlasova.iteco.taskmanager.api.endpoint.FindOneSessionResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.vlasova.iteco.taskmanager.api.endpoint.Session findOneSession(
        @WebParam(name = "arg0", targetNamespace = "")
        java.lang.String arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        java.lang.String arg1
    );
}
