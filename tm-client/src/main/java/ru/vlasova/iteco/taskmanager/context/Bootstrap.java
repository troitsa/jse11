package ru.vlasova.iteco.taskmanager.context;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.vlasova.iteco.taskmanager.api.endpoint.*;
import ru.vlasova.iteco.taskmanager.api.service.ServiceLocator;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.endpoint.*;
import ru.vlasova.iteco.taskmanager.service.StateService;
import ru.vlasova.iteco.taskmanager.service.TerminalService;
import java.lang.Exception;
import java.util.*;

@NoArgsConstructor
public final class Bootstrap implements ServiceLocator  {

    @Getter
    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpointService().getDomainEndpointPort();

    @Getter
    @NotNull
    private final StateService stateService = new StateService();

    @Getter
    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    @Getter
    @NotNull
    private final TerminalService terminalService = new TerminalService();

    @Getter
    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();

    @Getter
    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();

    @Getter
    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();

    private void registry(@NotNull final AbstractCommand command) throws Exception {
        @NotNull final String cliCommand = command.getName();
        @NotNull final String cliDescription = command.getDescription();

        if (cliCommand.isEmpty()) throw new Exception();

        if (cliDescription.isEmpty()) throw new Exception();

        command.setServiceLocator(this);
        stateService.add(command);
    }

    public void start() {
        createUsers();
        terminalService.print("*** WELCOME TO TASK MANAGER ***");
        @NotNull String command = "";

        while (true) {
            command = terminalService.readString();
            execute(command);
        }
    }

    private void execute(@Nullable final String command) {
        try {
            @Nullable final AbstractCommand abstractCommand = stateService.get(command);
            if (abstractCommand == null) return;
            @NotNull final boolean checkAccess = !abstractCommand.secure() || abstractCommand.secure() &&
                    stateService.getSession() != null;
            @Nullable final List<Role> roles = abstractCommand.getRole();
            @NotNull final boolean checkRole = roles == null ||
                    userEndpoint.checkRole(stateService.getSession().getUserId(), roles);
            if (checkAccess && checkRole) {
                abstractCommand.execute();
            } else {
                terminalService.print("Access denied. Please, login");
            }
        } catch (Exception e) {
            terminalService.print(e.getMessage());
        }

    }

    @NotNull
    public Bootstrap init() {
        @NotNull final Set<Class<? extends AbstractCommand>> classes = new TreeSet<>(Comparator.comparing(Class::getName));
        classes.addAll(new Reflections("ru.vlasova.iteco.taskmanager").getSubTypesOf(AbstractCommand.class));

        for (@NotNull Class clazz : classes) {
            try {
                if (AbstractCommand.class.isAssignableFrom(clazz)) {
                    registry((AbstractCommand) clazz.newInstance());
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        return this;
    }

    private void createUsers() {
        try {
            @NotNull final User user = userEndpoint.insertUser("user", "qwerty");
            user.setRole(Role.USER);

            @NotNull final User admin = userEndpoint.insertUser("admin", "1234");
            admin.setRole(Role.ADMIN);
            userEndpoint.persistUser(user);
            userEndpoint.persistUser(admin);
            Session session = sessionEndpoint.createSessionNewUser(user.getLogin(), user.getPwd());
            stateService.setSession(session);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}